# syntax = docker/dockerfile:experimental
FROM ros:melodic-ros-base

ENV ROS_VER $ROS_DISTRO

RUN apt-get update -y && apt-get install ros-$ROS_VER-realsense2-camera python3-pip libfreetype6-dev pkg-config -y 
RUN pip3 install --upgrade pip setuptools wheel

ADD ./requirements.txt ./
RUN --mount=type=cache,target=/root/.cache/pip \
    pip3 install -r requirements.txt

ADD ./ ./
