#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import sys

from StageDefinition.Executor import Executor
from StageDefinition.Stages.Discriminator import DiscriminatorStage
parser = argparse.ArgumentParser()


modes = ['image', 'video', 'ros']

parser.add_argument('--mode', required=True, default='image', choices=modes,
                    help='source of which kind should be processed')

first, rest = parser.parse_known_args()
mode = first.mode

if mode == 'video':
    from StageDefinition.Stages.ExporterVideoFile import ExporterStageVideoFile
    from StageDefinition.Stages.ImporterVideoFile import ImporterStageVideoFile
    pipeline = [
        ImporterStageVideoFile(),
        DiscriminatorStage(),
        ExporterStageVideoFile()
    ]
elif mode == 'ros':
    from StageDefinition.Stages.ImporterRos import ImporterStageRos
    from StageDefinition.Stages.ExporterRos import ExporterStageRos
    pipeline = [
        ImporterStageRos(),
        DiscriminatorStage(),
        ExporterStageRos()
    ]
else:
    from StageDefinition.Stages.ImporterFile import ImporterStageFile
    from StageDefinition.Stages.ExporterFile import ExporterStageFile
    pipeline = [
        ImporterStageFile(),
        DiscriminatorStage(),
        ExporterStageFile()
    ]


executor = Executor(pipeline)
arguments = executor.arguments

for name, arg in arguments.items():
    if not arg:
        continue
    parser.add_argument('--{0}'.format(name), **arg)
args = vars(parser.parse_args())
executor.apply_arguments(args)
executor.start()
