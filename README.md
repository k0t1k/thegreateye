## Mask Detection in a crowd with drones

This project uses YoLo3 neural net to detect people not wearing masks. It consists of two main modules
* Pipeline
* Neural net socket server

## Pipeline
This module is meant to be executed on the drone. It simply takes images from various sources, like
* pre-recorded video file (`--mode video ./src/file_in ./tgt/file_out`)
* directory with images (`--mode image ./src/ ./tgt/`)
* D435 camera (`--mode ros`)

and sends them to the neural net socket server (which can be launched on the same machine, this is why I've added torch to the container) and then writes the result to the corresponding destination (ROS topic for ros, output file for )

The operation mode described above is determined by `--mode` CLI argument, e.g.

`python main.py --mode ros`

if you want to work with D435 camera. This is the main operational mode, two others are for debugging and video processing

This module is containerized, you should build the container on the target machine, enter the container and run the command above. There are two dockerfiles - `x86.Dockerfile` is for x86 machines and `arm.Dockerfile` is for Jetson NX (Jetpack 4.4+ required)

All the Pipeline-related items are in directory `StageDefinition`

## Socket Server
The module the Pipeline communicates with. It can be launched alongside the Pipeline or on the different machine. You should provide the ip and port to listen on (otherwise it will fall back to defaults - localhost and 20000) 

`python discriminator.py --ip localhost --port 20001`


## Training
For training please use dataset like VOC,please download it (and transform if needed) and put right into the root of the project, then run

`python trainfile.py`

You might want to tweak some parameters, like batch size and learning rate - please, use the command-line arguments as they are presented inside `trainfile.py`
