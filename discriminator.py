import argparse
from time import sleep

from Model.models import Darknet
from Model.utils import non_max_suppression
from numpysocket.numpysocket import NumpySocket
import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import cv2
from PIL import Image

"""
This file runs socket server that waits for connection from the drone, 
processes incoming images, draws bounding boxes on them and 
sends the result back
"""

parser = argparse.ArgumentParser()

parser.add_argument("--model_def", type=str, default="config/yolov3_mask.cfg",
                    help="path to model definition file")

parser.add_argument("--data_config", type=str, default="data/mask_dataset.names",
                    help="path to data config file")

parser.add_argument("--weights", type=str, default="checkpoints/yolov3_ckpt.pth",
                    help="if specified starts from checkpoint model")

parser.add_argument("--port", type=int, default=20000,
                    help="Port to run the socket server on")

parser.add_argument("--img_size", type=int, default=416,
                        help="size of each image dimension")

opt = parser.parse_args()
print(opt)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


print(device)
model = Darknet(opt.model_def, img_size=opt.img_size).to(device)
weights = torch.load(opt.weights, map_location=device)

model.load_state_dict(weights)
model.eval()
classes = open(opt.data_config, "r").read().split("\n")[:-1]  # last is empty


def square_image(img, inp_dim):
    """
    resize image with unchanged aspect ratio using padding
    """
    img_w, img_h = img.shape[1], img.shape[0]
    w, h = inp_dim
    new_w = int(img_w * min(w / img_w, h / img_h))
    new_h = int(img_h * min(w / img_w, h / img_h))

    image = Image.fromarray(img, 'RGB')
    resized_image = np.asarray(image.resize((new_w, new_h)))
    canvas = np.asarray(image.resize((inp_dim[1], inp_dim[0]))).copy()
    canvas[(h - new_h) // 2:(h - new_h) // 2 + new_h,
    (w - new_w) // 2:(w - new_w) // 2 + new_w, :] = resized_image

    return canvas


def detect_image(image):
    conf_thresh = 0.8  # prob that belongs to some class, below thresh are ignored
    nms_thresh = 0.3  # cure the problem of multiple detection
    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor  # check cuda
    text_size = cv2.getTextSize("", cv2.FONT_HERSHEY_PLAIN, 2, 2)[0]  # text, fontFace, fontScale, thickness

    img_height, img_width = image.shape[:-1]  # height, width, 3 channels
    x = y = max(img_height, img_width)  # take the max
    start_img_height = int((y - img_height) / 2)
    start_img_width = int((x - img_width) / 2)
    img = square_image(image, [opt.img_size, opt.img_size])  # resize to make square
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # to rgb
    # then we normalizes, transpose and convert to Tensor
    img = Variable(torch.Tensor(
        np.expand_dims(np.transpose(np.asarray(img) / 255, [2, 0, 1]),
                       axis=0)).type(Tensor))  # ->color, x_coord, y_coord
    with torch.no_grad():  # for inference
        detections = model(img)
    detections = non_max_suppression(detections, conf_thresh, nms_thresh)
    mul_constant = x / opt.img_size
    for detection in detections:
        if detection is not None:
            for x1, y1, x2, y2, conf, cls_conf, cls_pred in detection:
                #                     print(cls_pred, x1,y1,x2,y2)
                x1 = int(x1 * mul_constant - start_img_width)
                y1 = int(y1 * mul_constant - start_img_height)
                x2 = int(x2 * mul_constant - start_img_width)
                y2 = int(y2 * mul_constant - start_img_height)
                bbw = x2 - x1
                bbh = y2 - y1
                #                     print(cls_pred, x1,y1,x2,y2)
                if int(cls_pred) == 0:
                    cv2.rectangle(image, (x1, y1), (x2, y2), (87, 245, 66), 2)
                else:
                    cv2.rectangle(image, (x1, y1), (x2, y2), (112, 48, 230), 2)
                #                     print(len(classes))
                #                     print(cls_pred)
                cv2.putText(image, classes[int(cls_pred)],
                            (x1, y1 + text_size[1] + 4),
                            cv2.FONT_HERSHEY_SIMPLEX, 1,
                            [225, 255, 255], 2)
            print(f'Found {str(len(detection))} faces')
    return image


comm_in = NumpySocket()
print('Ready to roll')
comm_in.startServer(20000)

while True:
    data = comm_in.recieveNumpy()
    comm_in.sendNumpy(detect_image(data)[:, :, ::-1])
