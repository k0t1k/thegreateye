 # -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from itertools import chain


class Executor(object):
    stages = None
    arguments = None

    def __init__(self, stages):
        self.stages = stages
        self.arguments = dict(
            chain.from_iterable(d.META.items() for d in self.stages))
        for i, stage in enumerate(self.stages[:-1]):
            stage.next_stage = self.stages[i+1]

    def apply_arguments(self, args):
        arg_mapping = {}
        for stage in self.stages:
            for key in stage.META:
                if key in arg_mapping and (arg_mapping[key][-1]).__name__ \
                        != type(stage).__name__:
                    raise ValueError(
                        '{0} and {1} share the same CLI argument: {2}'.
                        format(type(arg_mapping[key]).__name__,
                               type(stage).__name__, key))
                elif key in arg_mapping \
                        and type(arg_mapping[key][-1]).__name__ == type(
                        stage).__name__:
                    arg_mapping[key].append(stage)
                else:
                    arg_mapping[key] = [stage]
        for arg, values in arg_mapping.items():
            for value in values:
                value.cli_args[arg] = args[arg]

    def start(self):
        for stage in self.stages:
            stage.setup()

        if self.stages[0].is_async:
            return

        self.stages[0].update()

        for stage in self.stages:
            stage.cleanup()
