# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

from StageDefinition.Messages.Messages import ImageMessage
from StageDefinition.Stages.BaseStage import BaseStage


class ImporterStageRos(BaseStage):
    META = {}
    is_async = True
    bridge = None
    state = 0

    def setup(self):
        self.bridge = CvBridge()
        rospy.init_node('thegreateye', anonymous=True)
        rospy.Subscriber('/camera/color/image_raw', Image, lambda x: self.advance(x))

    def advance(self, x):
        if self.state % 2 == 0:
            self.update(x)
            self.state += 1
        else:
            self.state += 1

    def process_data(self, data=None):
        cv_image = self.bridge.imgmsg_to_cv2(data)
        yield ImageMessage(image=cv_image)
