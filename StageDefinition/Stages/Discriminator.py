# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from numpysocket.numpysocket import NumpySocket

from StageDefinition.Stages.BaseStage import BaseStage


class DiscriminatorStage(BaseStage):
    META = {
        'ip': {
            'help': 'the address of socket server',
            'default': 'localhost',
        },
        'port': {
            'help': 'the port of socket server',
            'default': 20000,
            'type': int
        }
    }

    comm = None

    def __init__(self):
        super(DiscriminatorStage, self).__init__()
        self.comm = NumpySocket()

    def setup(self):
        self.comm.startClient(self.cli_args['ip'], self.cli_args['port'])

    def process_data(self, data=None):
        self.comm.sendNumpy(data.image)
        data.image = self.comm.recieveNumpy()
        yield data
