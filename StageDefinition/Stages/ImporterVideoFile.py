# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import glob
import os

import cv2

from StageDefinition.Messages.Messages import ImageMessage
from StageDefinition.Stages.BaseStage import BaseStage


class ImporterStageVideoFile(BaseStage):
    META = {
        'in': {
            'required': True,
            'help': 'input file path'
        }
    }

    file = None

    def setup(self):
        self.file = cv2.VideoCapture(self.cli_args['in'])

    def process_data(self, data=None):
        while self.file.isOpened():
            ret, frame = self.file.read()
            print('frame')
            # if frame is read correctly ret is True
            if not ret:
                print("Can't receive frame (stream end?). Exiting ...")
                break
            yield ImageMessage(image=frame)

    def cleanup(self):
        self.file.release()
