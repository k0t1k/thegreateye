# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import glob
import os

import cv2

from StageDefinition.Messages.Messages import ImageMessage
from StageDefinition.Stages.BaseStage import BaseStage


class ImporterStageFile(BaseStage):
    META = {
        'in': {
            'required': True,
            'help': 'input directory'
        }
    }

    def process_data(self, data=None):
        for i, img_name in enumerate(
                glob.glob(os.path.join(self.cli_args['in'], '*'))):
            img = cv2.imread(img_name, cv2.IMREAD_COLOR)
            name = os.path.split(img_name)
            yield ImageMessage(
                image=img,
                name=name[1])

