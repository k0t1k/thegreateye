# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import os

from StageDefinition.Stages.BaseStage import BaseStage
import numpy as np
import cv2


class ExporterStageFile(BaseStage):
    META = {
        'out': {
            'help': 'output directory',
            'required': True
        }
    }

    index = 0

    def setup(self):
        os.makedirs(self.cli_args['out'], exist_ok=True)

    def process_data(self, data=None):
        cv2.imwrite(os.path.join(self.cli_args['out'], data.name), data.image)
