# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import os
from StageDefinition.Stages.BaseStage import BaseStage
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge


class ExporterStageRos(BaseStage):
    META = {}
    pub = None
    bridge = None

    def setup(self):
        self.bridge = CvBridge()
        self.pub = rospy.Publisher('maskedfaces', Image, queue_size=5)
        rospy.spin()

    def process_data(self, data=None):
        img = self.bridge.cv2_to_imgmsg(data.image)
        img.encoding = 'bgr8'
        self.pub.publish(img)
