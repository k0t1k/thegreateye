# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import glob
import os

import cv2

from StageDefinition.Messages.Messages import ImageMessage
from StageDefinition.Stages.BaseStage import BaseStage


class ExporterStageVideoFile(BaseStage):
    META = {
        'out': {
            'required': True,
            'help': 'output file path'
        }
    }

    out = None

    def setup(self):
        fourcc = cv2.VideoWriter_fourcc(*'DIVX')
        self.out = cv2.VideoWriter(self.cli_args['out'], fourcc, 25.0, (1920, 1080))

    def process_data(self, data=None):
        self.out.write(data.image)

    def cleanup(self):
        self.out.release()
