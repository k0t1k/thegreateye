# -*- coding: utf-8 -*-


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from multiprocessing import Pipe


class BaseStage(object):
    """
    This is a base class for all the Stages
    It is not very useful, since it does nothing

    If you want to accept some meta-arguments from CLI - register them in the
    META variable, like below

    META = {
        'arg_name': {
            'type':          type of returned argument
            'required'       whether this is mandatory arg
            # This dict will be passed to argparse.addArgument
            # So other modifiers here (like help) are applicable
            # This dict can be empty/None as well
        }
    }
    """
    META = {}
    cli_args = None
    is_async = False

    def __init__(self, next_stage=None):
        self.cli_args = {}
        self.next_stage = next_stage

    def setup(self):
        """
        The function to run before execution of the main data processing
        :return:
        """

    def cleanup(self):
        """
        The function to run after execution of the main data processing
        :return:
        """

    def process_data(self, data=None):
        """
        Processes incoming data stream

        :param data: data items in any format, as long as this
                     Stage can understand the previous stage
                     Can be empty in case of Importer Stage

        :return: an generator of processed items in any format,
                 as long as the next Stage
                 can understand iterator elements as a valid data input
        """
        pass

    def update(self, data=None):
        result = self.process_data(data)
        if result is not None and self.next_stage is not None:
            for x in result:
                self.next_stage.update(x)

